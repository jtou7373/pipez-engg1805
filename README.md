# README #

Pipez codebase for ENGG1805.

Pipez is a simple system for executing workflows that do text transformations. A workflow is defined as
a pipeline of one or more pipes, each of which performs a specific transformation. Hence, we are able
to perform a variety of text transformations by combining pipes into different combinations. The Pipez
system takes an input of text, and using one or more pipes configured into a pipeline, creates a
workflow that changes the input text to a new output format. We will be working with Pipez using only
comma-separated (CSV) files because they will be easiest to work with.

Pipez has been designed to illustrate key concepts taught in this subject, e.g. unit testing (of pipes),
integration testing (of pipelines), decomposition of a large problem into simpler parts and reuse (of
tested pipes). You will also have the opportunity to extend the system by writing your own pipes.

The Pipez system forms the basis of the tutorial work for this course and working with it will also allow
you to learn concepts such as version control, collaboration on an IT project, issue tracking, etc. Please
note that the work we do on Pipez in each tutorial will also lead on to the course assignment and you
should be clear that you understand how Pipez works – please ask any questions to clarify as they come
up!

asdf

Feel free to resolve / add issues at your leisure